import axios from "axios";

export default function apiPlatform(config={}){
    const defaultConfig = {
        baseURL: "https://localhost:8443/",
        headers: {
            Accept: "application/json",
        },
    };

    if(config.auth){
        const token = localStorage.getItem("token_api_plat");
        defaultConfig.headers.Authorization = `Bearer ${token}`;
    }

    return axios.create({ ...defaultConfig });
}
