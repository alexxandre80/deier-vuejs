import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        status: '',
        enabled: !!localStorage.getItem('token_api_plat') && !!localStorage.getItem('idPrisma'),
        tokenApiPlat: localStorage.getItem('token_api_plat') || '',
        tokenPrisma: localStorage.getItem('USER_TOKEN') || '',
        user: localStorage.getItem('userInfo') || '',
        idPrisma: localStorage.getItem('idPrisma') || '',
        currentApi: localStorage.getItem('CURRENT-API') || '',
        role: JSON.parse(localStorage.getItem('roles')) || '',
    },

    getters: {
        isLoggedIn: state => !!state.tokenApiPlat && !!state.tokenPrisma,
        authStatus: state => state.status,
        api : state => state.currentApi,
        idUserPrisma: state => state.idPrisma,
        userApiPlat: state => state.user,
        role: state => state.role,
        isEleveur: state => state.role.indexOf('ROLE_ELEVEUR') !== -1,
        isSeller: state => state.role.indexOf('ROLE_SELLER') !== -1,
        isZoo: state => state.role.indexOf('ROLE_ZOO') !== -1,
        isAdmin: state => state.role.indexOf('ROLE_ADMIN') !== -1,
        isUser: state => state.role.indexOf('ROLE_USER') !== -1 && state.role.length === 1
    },

    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state) {
            state.status = 'success'
            state.tokenApiPlat = localStorage.getItem("token_api_plat");
            state.user = localStorage.getItem("userInfoApiPlat");
        },
        auth_success_prisma(state) {
            state.tokenPrisma = localStorage.getItem('USER_TOKEN')
            state.idPrisma = localStorage.getItem('idPrisma')
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            state.status = ''
            state.tokenApiPlat = ''
            state.tokenPrisma = ''
            state.idPrisma = ''
            state.enabled = false
        },
        connexion(state){
            state.enabled = true
        },
        switch_api(state){
            state.currentApi = localStorage.getItem('CURRENT-API')
        }
    },

    actions: {
        logout({commit}) {
            commit('logout');
            localStorage.removeItem('token_api_plat');
            localStorage.removeItem("userInfoApiPlat");
            localStorage.removeItem('USER_TOKEN');
            localStorage.removeItem('idPrisma');
        }

    }
});