import gql from 'graphql-tag'

export const UPDATE_TRANSAC_REFUSE = gql`
    mutation UpdateTransacRefuse(
        $id: ID!
    ) {
        updateTransaction(where: {id : $id}
            data: {
                statut: 2
            }
        ) {
            type
            prix
            statut

        }
    }
`
export const UPDATE_TRANSAC_ACCEPTE = gql`
    mutation UpdateTransacAccept(
        $id: ID!
        $idNewProprio: ID!

    ) {
        updateTransaction(where: {id : $id}
            data: {
                statut: 1
                animals:{update:{proprio:{connect:{
                    id: $idNewProprio
                }}}}
            }

        ) {
            type
            prix
            statut

        }
    }
`





