import gql from 'graphql-tag'

export const SIGNUP_MUTATION = gql`
    mutation SignupMutation($email: String!, $password: String!, $name: String!, $adresse: String!, $cp: String!, $ville: String!, $pays: String!, $telephone: String!) {
        signup(email: $email, password: $password, name: $name, adresse: $adresse, cp: $cp, ville: $ville, pays: $pays, telephone: $telephone ) {
            token
        }
    }
`
export const LOGIN_MUTATION = gql`
    mutation LoginMutation($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            token
        }
    }
`
export const CREATE_ANIMAL_MUTATION = gql`
    mutation CreateAnimalMutation($espece: String!, $race: String!, $genre: String!, $dateBirth: DateTime!, $image: String!, $birthplace: String!, $prix: String!
    ) {
        createAnimal(
            data: {espece: $espece
                  race: $race
                  genre: $genre
                  image: $image
                  prix: $prix
                  dateBirth: $dateBirth
                  birthplace: $birthplace
                  proprio: {connect: {id: "1"}}
                  
            }
            
        ) {
            id
            espece
            race
            genre
            image
            prix
            dateBirth
            birthplace
            
           
        }
    }
`

export const UPDATE_MUTATION = gql`
    mutation updateMutation($email: String!,  $name: String!, $adresse: String!, $cp: String!, $ville: String!, $pays: String!, $telephone: String!
    ) {
        updateUser(where: { id: "ckk1gk75c00410825qynbq5k0"}
            data: {
                email: $email
                name: $name
                adresse: $adresse
                cp: $cp
                ville: $ville
                pays: $pays
                telephone: $telephone

            }

        ) {
            id
            email
            password
            name
            adresse
            cp
            ville
            pays
            telephone


        }
    }
`


export const CREATE_TRANSAC_MUTATION = gql`
    mutation CreateTransacMutation(
        $type: String!
        $prix: String!
        $animals: ID!
        $vendeur: ID!
    ) {
        createTransaction(
            data: {
                type: $type
                prix: $prix
                animals: { connect: { id: $animals }}
                vendeur: { connect: { id: $vendeur }}
                acheteur: { connect: { id: 1 }}
            }
        ) {
            id
            type
            prix

        }
    }
`

export  const DELETE_ANIMAL = gql`
    mutation deleteAnimal($id: ID!) {
        deleteAnimal(where: {id: $id}) {
            id
        }
    }
    `
export const CREATE_CERTIF_MUTATION = gql`
    mutation CreateCertifMutation(
        $nom: String!
        $dateObtention: DateTime!
    ) {
        createCertification(
            data: {
                nom: $nom
                user:{connect:{id: "1"}}
                validation: false
                dateObtention: $dateObtention
            }
        ) {
            id
            nom
            dateObtention
        }
    }
    
`
export const UPDATE_ANIMAL_MUTATION = gql`
mutation updateAnimalMutation(
  $id: ID!
  $espece: String!
  $race: String!
  $genre: String!
  $prix: String!
  $dateBirth: DateTime!
  $birthplace: String
) {
  updateAnimal(
    where: { id: $id }
    data: {
      espece: $espece
      race: $race
      genre: $genre
      prix: $prix
      dateBirth: $dateBirth
      birthplace: $birthplace
    }
  ) {
    id
    espece
    race

    genre
    prix
    dateBirth
    birthplace
  }
}

`

export const CREATE_COMMENTAIRE_MUTATION = gql`
    
    mutation CreateCommentaireMutation(
        $comment: String
        $note: String!
        $user: ID!
    ) {
        createCommentaire(
            data: {
                comment: $comment
                note: $note
                user: { connect: { id: $user }}
                author: { connect: { id: 1 }}
            }
        ) {
            id
            note
            comment
        }
    }
    `


