
import gql from 'graphql-tag'

export const ANIMALS_QUERY = gql`
    query AnimalsQuery {
        animals {
            id
            espece
            race
            genre
            prix
            dateBirth
            birthplace
            proprio {
                id
                name
                email
            }
           
        }
    }
`
export const SINGLE_ANIMAL_QUERY = gql`
    query SingleAnimalQuery($id: ID!) {
        animal(where: {id: $id}) {
            id
            espece
            race
            genre
            prix
            dateBirth
            birthplace
            proprio {
                id
                name
                email
                adresse
                cp
                ville
                pays
            }
        }
    }
    
`

export const ME_QUERY = gql`
    query MeQuery {
        me {
            id
            name
            email
            telephone
            adresse
            ville
            cp
            pays
            nameZoo
        }
    }
`

export const USER_QUERY = gql`
query SingleTransacQuery($id: ID!) {
    user(where: {id: $id}) {

        id
        name
        email
        telephone
        adresse
        ville
        cp
        pays
        nameZoo

    }
}
`

export const SINGLE_TRANSAC_QUERY = gql`
    query SingleTransacQuery($id: ID!) {
        transaction(where: {id: $id}) {
            id
            type
            prix
            statut
            animals{
                id
                espece
                genre
                prix
                race
            }
            vendeur {
                id
                name
                email
                adresse
                cp
                ville
                pays
            }
            acheteur{
                id
            }

        }
    }
`

export const TRANSAC_QUERY = gql`
    query TransacQuery($id: ID!) {

        transactions(where:{OR: [{vendeur:{id:$id}},{acheteur:{id:$id}}]}){
            type
            statut
            prix
            id
            animals{
                espece
                race
                genre
                prix

            }
            acheteur{
                id
                name}
            vendeur{
                id
                name}
        }

        
    }
`

export const YOUR_ANIMALS_QUERY = gql`
    query YourAnimals($id: ID) {

        animals(where:{proprio:{id: $id}}){
            id
            espece
            prix
            race
            genre
            dateBirth
            birthplace
        }
    }
`
export const YOUR_CERTIFS_QUERY = gql`
    query YourCertifs($id: ID) {

        certifications(where:{user:{id: $id}}){
            nom
            dateObtention
        }
    }
`

export const YOUR_COMMENTAIRES_QUERY = gql`
    query YourCommentaires($id: ID) {

        commentaires(where:{user:{id: $id}}){
            comment
            note
        }
    }
`

