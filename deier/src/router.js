import Vue from 'vue';
import Router from 'vue-router';
import store from './store/store.js';
import Index from './pages/Index.vue';
import Landing from './pages/Landing.vue';
import Login from './pages/Login.vue';
import Profil from './pages/Profil.vue';
import MainNavbar from './layout/MainNavbar.vue';
import MainFooter from './layout/MainFooter.vue';
import Signup from './pages/Signup.vue'
import AnimalList from './pages/components/AnimalList.vue';
import CreateAnimal from "@/pages/CreateAnimal";
import SingleAnimal from "@/pages/components/SingleAnimal";
import CreateTransac from "@/pages/CreateTransac";
import SingleTransac from "@/pages/components/SingleTransac";
import TransactionList from "@/pages/components/TransactionList";
import CreateCertif from "@/pages/CreateCertif";
import SingleProfil from "@/pages/components/SingleProfil";
import UpdateAnimal from "@/pages/UpdateAnimal";

Vue.use(Router);

const router =  new Router({
  linkExactActiveClass: 'active',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      components: { default: Index, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/signup',
      name: 'SignUp',
      components: { default: Signup, header: MainNavbar},
      props: {
        header: { colorOnScroll: 100 }
      }
    },
    {
      path: '/login',
      name: 'LogIn',
      components: { default: Login, header: MainNavbar},
      props: {
        header: { colorOnScroll: 100 }
      }
    },
    {
      path: '/animal',
      name: 'animal',
      components:  { default: AnimalList, header: MainNavbar, footer: MainFooter},
      meta: { requiresAuth: true  },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/transaction',
      name: 'transaction',
      components:  { default: TransactionList, header: MainNavbar, footer: MainFooter},
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/createanimal',
      name: 'CreateAnimal',
      components: { default:CreateAnimal, header: MainNavbar},
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },

    {
      path: '/createCertif',
      name: 'CreateCertif',
      components: { default:CreateCertif, header: MainNavbar},
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },


    {
      path: '/createtransac/:id',
      name: 'CreateTransac',
      components: { default:CreateTransac, header: MainNavbar, footer: MainFooter},
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/demoTemp',
      name: 'landing',
      components: { default: Landing, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/profil',
      name: 'profil',
      components: { default: Profil, header: MainNavbar, footer: MainFooter },
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      },
    },
    {
      path: '/animal/:id',
      name: 'SingleAnimal',
      components: { default: SingleAnimal, header: MainNavbar, footer: MainFooter },
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/transac/:id',
      name: 'SingleTransac',
      components: { default: SingleTransac, header: MainNavbar, footer: MainFooter },
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/profil/:id',
      name: 'SingleProfil',
      components: { default: SingleProfil, header: MainNavbar, footer: MainFooter },
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },

    {
      path: '/profil/updateanimal/:id',
      name: 'UpdateAnimal',
      components: { default: UpdateAnimal, header: MainNavbar, footer: MainFooter },
      meta: { requiresAuth: true },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: 'black' }
      }
    },



  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router;