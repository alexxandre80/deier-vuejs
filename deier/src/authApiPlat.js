import apiPlatform from "@/api/ApiPlatform";
import jwt_decode from "jwt-decode";

export function login(email, password) {
    const api = apiPlatform();
    return api
        .post('authentication_token', {
            'email': email,
            'password': password
        })
        .then((response) => {
            const {data} = response;
            if (data.token) {
                localStorage.setItem("token_api_plat", data.token);
                let decodedToken = jwt_decode(data.token);
                const userInfo = {
                    id: decodedToken.id,
                    name: decodedToken.name,
                    roles: decodedToken.roles,
                    email: decodedToken.username
                }
                localStorage.setItem("roles", JSON.stringify(decodedToken.roles));
                localStorage.setItem("userInfoApiPlat", JSON.stringify(userInfo));
                return;
            }
            throw new Error("Token manquant");
        })
        .catch(function (e) {
            console.error(e.message);
        });
}

export function signup(props, idPrisma) {
    const api = apiPlatform();
    const formData = new FormData();
    formData.append('name', props.name)
    formData.append('email', props.email)
    formData.append('password', props.password)
    formData.append('adresse', props.adresse)
    formData.append('cp', props.cp)
    formData.append('ville', props.ville)
    formData.append('pays', props.pays)
    formData.append('telephone', props.telephone)
    formData.append('prismaId', idPrisma)
    formData.append('role', props.role)
    return api
        .post('register', formData)
        .then(() => {
            return login(props.email, props.password)
        })
        .catch(function (e) {
            console.error(e.message);
        });
}